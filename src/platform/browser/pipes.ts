/*
 * These are globally available pipes in any template
 */

import { PLATFORM_PIPES } from '@angular/core';

// Angular 2 Material
// TODO(gdi2290): replace with @angular2-material/all
import {MATERIAL_PIPES} from './angular2-material2';
// application_pipes: pipes that are global through out the application
export const APPLICATION_PIPES = [
...MATERIAL_PIPES
];

export const PIPES = [
  {provide: PLATFORM_PIPES, multi: true, useValue: APPLICATION_PIPES }
];
