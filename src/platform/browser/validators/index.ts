/**
 * Created by Manu on 5/4/2016.
 */
import {EmailValidator} from "./email.validator";
import {EqualValidator} from "./equal.validator";

export const VALIDATOR_DIRECTIVES = [
  ...[
    EqualValidator,
    EmailValidator
  ]
];
