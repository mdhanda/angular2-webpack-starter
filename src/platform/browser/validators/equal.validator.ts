/**
 * Created by Manu on 5/4/2016.
 */
import {provide, Directive, forwardRef} from 'angular2/core';
import {Control, ControlGroup, NG_VALIDATORS} from 'angular2/common';

function validateEqualFactory() {
  return ({value}: ControlGroup): {[key: string]: any} => {

    const [first, ...rest] = Object.keys(value || {});
    const valid = rest.every(v => value[v] === value[first]);
    return valid ? null : {
      validateEqual: {
        valid: false
      }
    };
  };
}

@Directive({
  selector: '[validateEqual][ngControl],[validateEqual][ngModel],[validateEqual][ngFormControl]',
  providers: [
    provide(NG_VALIDATORS, {
      useExisting: forwardRef(() => EqualValidator),
      multi: true
    })
  ]
})
export class EqualValidator {

  validator: Function;

  constructor() {
    this.validator = validateEqualFactory();
  }

  validate({value}: ControlGroup) {
    return this.validator({value});
  }
}
