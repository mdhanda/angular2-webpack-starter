/*
 * Angular 2 decorators and services
 */
import {Component, ViewEncapsulation} from '@angular/core';
import {RouteConfig, Router} from '@angular/router-deprecated';
import {Control, ControlGroup, FormBuilder, Validators} from '@angular/common';

import {Home} from './home';
import {AppState} from './app.service';
import {RouterActive} from './router-active';
import ValidationService from "./common/services/validation.service";
import AuthService from "./common/services/auth.service";
import UserService from "./common/services/user.service";

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  pipes: [ ],
  providers: [ AuthService, UserService ],
  directives: [ RouterActive ],
  encapsulation: ViewEncapsulation.None,
  styles: [
    require('./app.css')
  ],
  template: require('./app.html')
})
@RouteConfig([
  { path: '/',      name: 'Index', component: Home, useAsDefault: true },
  { path: '/home',  name: 'Home',  component: Home },
  // Async load a component using Webpack's require with es6-promise-loader and webpack `require`
  { path: '/about', name: 'About', loader: () => require('es6-promise!./about')('About') }
])
export class App {
  angularclassLogo = 'assets/img/angularclass-avatar.png';
  instacardsLogo = 'assets/img/logo.png';
  loading = false;
  name = 'Instacards';
  url = 'http://www.instacards.com.au';

  registerFormModel: ControlGroup;
  loginFormModel: ControlGroup;

  selectedAuthProvider: string = 'instagram';
  authProviders = [
    'local',
    'facebook',
    'instagram',
    'google',
  ];


  constructor(
    public appState: AppState,
    public authService: AuthService,
    public userService: UserService
  ) {

    const registerFormBuilder: FormBuilder = new FormBuilder();
    const loginFormBuilder: FormBuilder = new FormBuilder();

    this.registerFormModel = registerFormBuilder.group({
      'email': ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
      'passwordsGroup': registerFormBuilder.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(5)])],
        'pconfirm': ['', Validators.required]
      }, {validator: ValidationService.matchingPasswords('password', 'pconfirm')})
    });

    this.loginFormModel = loginFormBuilder.group({
      'authProvider': ['', Validators.required],
      'email': ['', Validators.required],
      'password': ['', Validators.minLength(5)]
    });
  }

  ngOnInit() {
    console.log('Initial App State', this.appState.state);
  }

  onRegister() {
    console.log('onRegister', this.registerFormModel.value['email'] +' - '+this.registerFormModel.value['password'] +' - '+this.registerFormModel.valid);
    if (this.registerFormModel.valid) {
      this.appState.set('email', this.registerFormModel.value['email']);
      this.appState.set('password', this.registerFormModel.value['password']);
    } else {
      if(this.registerFormModel.hasError('matchingPasswords')) {
        this.appState.set('error', 'Password mismatch.');
      }
    }
  }

  onLogin() {
    console.log('onLogin', this.loginFormModel.value['email'] +' - '+this.loginFormModel.value['password'] +' - '+this.loginFormModel.valid);
    if (this.loginFormModel.valid) {
      const authP: string = this.loginFormModel.value['authProvider'];
      const email: string = this.loginFormModel.value['email'];
      const password: string = this.loginFormModel.value['password'];
      this.appState.set('authP', authP);
      this.appState.set('email', email);
      this.appState.set('password', password);
      switch (authP)
      {
        case'facebook':
          this.authService.authenticateFacebook({email, password});
          break;
        case'instagram':
          this.authService.authenticateInstagram({email, password});
          break;
        case'google':
          alert("In google");
          break;
        default:
          this.authService.authenticateLocal({email, password});
      }
    } else {
      this.appState.set('error', 'Enter correct email/password.');
    }
  }

  insertUser() {
    console.log('insertUser', this.loginFormModel.value['email'] +' - '+this.loginFormModel.value['password'] +' - '+this.loginFormModel.valid);
      this.userService.create({
        email: 'manuraj.dhanda@gmail.com',
        password: 'manu123456'
      })
  }
}

/*
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
