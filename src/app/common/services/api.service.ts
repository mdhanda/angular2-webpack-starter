/**
 * Created by Manu on 5/2/2016.
 */
// common/services/api.ts

import {Injectable} from 'angular2/core';

const HOST = window.location.href; //"http://localhost:3000"; // Your base server URL here
declare const io;
declare const feathers;
declare const superagent;


@Injectable()
export class ApiService {
  private _feathersApp: any;
  private _url: string = HOST; //'http://instacards-cloudgroup.rhcloud.com'; //https://your-api-base-url.com';
  constructor() {
    console.log(`Host is: ${HOST}`);
  /*
    const socket = io(this.url);
    this.feathersApp = feathers()
      .configure(feathers.socketio(socket))
      .configure(feathers.hooks())
      .configure(feathers.authentication({ storage: window.localStorage }));
  */
    this._feathersApp = feathers()
      .configure(feathers.rest(this.url).superagent(superagent))
      .configure(feathers.hooks())
      .configure(feathers.authentication({ storage: window.localStorage }));
  }


  get url(): string {
    return this._url;
  }

  get feathersApp(): any {
    return this._feathersApp;
  }

}
