/**
 * Created by Manu on 5/2/2016.
 */
import {Injectable} from 'angular2/core';
import {Observable} from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';

import {User} from '../../common/models/user.model';
import {ApiService} from '../../common/services/api.service';

@Injectable()
export default class UserService extends ApiService {
  public users$: Observable<User[]>;
  private feathersService: any;
  private usersObserver: Observer<User[]>;
  private dataStore: {
    users: User[]
  };
  constructor() {
    super();

    this.feathersService = this.feathersApp.service('users');

    this.feathersService.on('created', (newUser) => this.created(newUser));
    this.feathersService.on('removed', (removedUser) => this.removed(removedUser));

    this.users$ = new Observable<User[]>(observer => this.usersObserver = observer)
      .share();

    this.dataStore = { users: [] };
  }

  public create(newUser) {
    this.feathersService.create(newUser).then(function(u) {
      console.log('Created new User', u);
    });
  }

  public find() {
    this.feathersService.find((err, users: User[]) => {
      if (err) return console.error(err);

      this.dataStore.users = users;
      this.usersObserver.next(this.dataStore.users);
    });
  }

  private getIndex(id: string): number {
    let foundIndex = -1;

    for (let i = 0; i < this.dataStore.users.length; i++) {
      if (this.dataStore.users[i].id === id) {
        foundIndex = i;
      }
    }

    return foundIndex;
  }

  private created(newUser: User) {
    const index = this.getIndex(newUser.id);

    if (index === -1) {
      this.dataStore.users.push(newUser);
    } else {
      this.dataStore.users[index] = newUser;
    }

    this.usersObserver.next(this.dataStore.users);
  }

  private removed(removedUser) {
    const index = this.getIndex(removedUser.id);

    this.dataStore.users.splice(index, 1);

    this.usersObserver.next(this.dataStore.users);
  }
}
