/**
 * Created by Manu on 5/2/2016.
 */
import {Injectable} from 'angular2/core';
import {Observable} from 'rxjs';

import {User} from '../../common/models/user.model';
import {ApiService} from '../../common/services/api.service';

@Injectable()
export default class AuthService extends ApiService {
  private dataStore: {
    users: User[]
  };
  constructor() {
    super();
  }

  public authenticateLocal(authObj:any) {
    this.feathersApp.authenticate({
      type:'token',
      'email': authObj.email,
      'password': authObj.password
    }).then(function(result){
      console.log('Authenticated!', result.token);
    }).catch(function(error){
      console.error('Error authenticating!', error);
    });
  }

  public authenticateInstagram(authObj:any) {
    console.log('In authenticateInstagram!', authObj.email+" - "+authObj.password);
    this.feathersApp.authenticate({
      'provider': 'instagram',
      'email': authObj.email,
      'password': authObj.password
    }).then(function(result){
      console.log('Authenticated!', result);
    }).catch(function(error){
      console.error('Error authenticating!', error);
    });
  }

  public authenticateFacebook(authObj:any) {
    console.log('In authenticateFacebook!', authObj.email+" - "+authObj.password);
    this.feathersApp.authenticate({
      'provider': 'facebook',
      'email': authObj.email,
      'password': authObj.password
    }).then(function(result){
      console.log('Authenticated!', result);
    }).catch(function(error){
      console.error('Error authenticating!', error);
    });
  }

  public authenticateGoogle(authObj:any) {
    this.feathersApp.authenticate({
      'provider': 'google',
      'email': authObj.email,
      'password': authObj.password
    }).then(function(result){
      console.log('Authenticated!', result);
    }).catch(function(error){
      console.error('Error authenticating!', error);
    });
  }

  public authenticateSocial(authObj:any) {
    console.log('In authenticateSocial!', authObj.email+" - "+authObj.password);
    this.feathersApp.authenticate({
      type:'oauth2',
      provider: 'facebook',
      'email': authObj.email,
      'password': authObj.password
    }).then(function(result){
      console.log('Authenticated!', this.feathersApp.get('token'));
    }).catch(function(error){
      console.error('Error authenticating!', error);
    });
  }

}
