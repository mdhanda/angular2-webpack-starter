/**
 * Created by Manu on 5/2/2016.
 */
export class Address {
  public id:any;
  public addressLine1: string;
  public addressLine2: string;
  public city: string;
  public state: string;
  public postcode: string;
}
