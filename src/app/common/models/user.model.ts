/**
 * Created by Manu on 4/27/2016.
 */
import {Address} from "./address.model";

export class User {
  public id:any;

  public facebookId:string;
  public facebook:any;

  public googleId:string;
  public google:any;

  public instagramId:string;
  public instagram:any;

  public paypalId:string;
  public paypal:any;

  public email:string;
  public password:string;

  public firstName:string;
  public lastName:string;
  public dob:Date;
  public created:any;
  public previousLogins:any[];

  public billingAddress:Address;
  public shippingAddress:Address;

  public fullName() {
    return this.firstName + ' ' + this.lastName;
  }

  public age() {
    let now:Date = new Date();
    return now.getFullYear() - this.dob.getFullYear();
  }
}
